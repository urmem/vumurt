package ru.iskander.javanewspring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.iskander.javanewspring.models.User;
import ru.iskander.javanewspring.services.UserService;

@Controller
@RequiredArgsConstructor

public class UserController {
    private final UserService userService;

    @GetMapping("/user/{user}")
    public String userInfo(@PathVariable("user")User user, Model model){
        model.addAttribute("user",user);
        model.addAttribute("products",user.getProducts());
        return  "user-info";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String createUser(User user, Model model) {
        if (!userService.createUser(user)) {
            model.addAttribute("Ошибка", "Пользователь с таким email "
                    + user.getEmail() + " уже существует");
            return "registration";
        }
        return "redirect:/login";
    }


    @GetMapping("/hello")
    public String securityUrl() {
        return "hello";
    }
}
