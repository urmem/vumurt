package ru.iskander.javanewspring.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.iskander.javanewspring.models.User;
import ru.iskander.javanewspring.models.enums.Role;
import ru.iskander.javanewspring.repositories.UserRepo;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {
    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;
    public boolean createUser(User user){
        String email = user.getEmail();
        if(userRepo.findByEmail(email) != null)return false;
        user.setActive(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.getRoles().add(Role.ROLE_USER);
        log.info("Новый пользователь сохранен:{}",email);
        userRepo.save(user);
        return true;
    }
    public List<User> list(){
        return userRepo.findAll();
    }

    public void banUser(Long id) {
        User user = userRepo.findById(id).orElse(null);
        if(user != null){
            if(user.isActive()){
                user.setActive(false);
                log.info("Бан пользователя по id = {}; email: = {}", user.getId(), user.getEmail());
            }else {
                user.setActive(true);
                log.info("Разбанить  пользователя по id = {}; email: = {}", user.getId(), user.getEmail());
            }
        }
        userRepo.save(user);
    }
    public void changeUserRoles(User user, Map<String, String> form){
        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());
        user.getRoles().clear();
        for(String key: form.keySet()){
            if(roles.contains(key)){
                user.getRoles().add(Role.valueOf(key));

            }
        }
        userRepo.save(user);
    }
}
