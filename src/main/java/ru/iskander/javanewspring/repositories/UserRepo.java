package ru.iskander.javanewspring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iskander.javanewspring.models.User;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
